local ent = ents.Derive("base")
--Loads the box
function ent:load( )
	--Size related Default values
        self.x= 122
        self.y= 125
        self.w=72;
        self.h=122;
        self.initialAndFixedY = y
        self.ang=0
    --Appearance Related
        self.colorR=1;
        self.colorG=1;
        self.colorB=1;
        self.transparency=1
        self.remainingAdd=1
        
        --workaround for bug
       self. passedCheckpoint=0
    self.creationStamp = love.timer.getTime() + math.random( 0, 128 )
    --Just to give like a weight style
        self.size=math.random(4,6)
    --Status
        self.falling=false
        self.initialHitPoint=math.random(1*level/10,250*level/10);
        self.initialHitPointFixed=self.initialHitPoint
        
        if math.random(1,72)==72 then
            self.initialHitPoint=math.random(270,1000)
            self.initialHitPointFixed=self.initialHitPoint
            self.remainingAdd=20
        end
        self.boxScore=self.initialHitPoint*120
        self.currentHitPoint=self.initialHitPoint;
end

--Spawns the Box with given properties
function ent:spawn(x,y,w,h,r,g,b,transparency)
    self:setPosition(x,y)
    self:setSize(w,h)
    self:setColor(r,g,b,transparency)
end

--Sets the appearance of the box
 function ent:setColor(colorR,colorG,colorB,transparency)

    self.colorR=colorR
    self.colorG=colorG
    self.colorB=colorB
    self.transparency=transparency
 end

--Set the width and height of the box
function ent:setSize( w, h )
	self.w = w
	self.h = h
end
--Sets the position
function ent:setPosition(x,y)
    self.x=x
    self.y=y
    self.initialAndFixedY=y
end

--Makes the box fall
function ent:Fall()
    self.falling=true
end

--Gets the position of the Box
function ent:getPosition()

    return self.x,self.y
end

--Gets the size of the box
function ent:getSize()
	return self.w, self.h;
end
--respawns the box after it passes the checkpoint
function ent:respawn()

self.x=math.random(-20,-5)
end

--Updates the boxes Position
function ent:update(dt)
	if self.x>=widthScreen-2 then
        self.passedCheckpoint=self.passedCheckpoint+1
        self:respawn()
       remainingPlayer=remainingPlayer-1*self.passedCheckpoint
     end
       
    
    if self.falling==false then
        self.x=self.x+(self.size*9)*dt
        self.y=(self.initialAndFixedY+math.sin(love.timer.getTime()-self.creationStamp)*(self.size*3))
    
    --When the box is falling
    elseif self.falling==true or self.passedCheckPoint>4 then
        self.x=self.x+(self.size*9)*dt*12
        self.y=self.y+1200*dt
        self.initialAndFixedY=self.initialAndFixedY+32*dt
        self.ang=self.ang+math.pi*0.025*dt
        if self.y>=300 then
            playerScore=playerScore+self.initialHitPointFixed
            enemyCount=enemyCount-1
            remainingPlayer=math.ceil(remainingPlayer+self.remainingAdd/(self.passedCheckpoint+1));
            startBGExplosion(self.x+512*(self.size/20),self.y+128*(self.size/20),1)
            self.falling=false
            --print(self.id)
            ents.Destroy(self.id)
        end
    end
end

function ent:damage(damage)
    
    local currentHp=self.currentHitPoint
    if(damage>=currentHp) then
        self.currentHp=0
        self:Fall();
        self.initialHitPoint=0
    
    else
        self.currentHitPoint=self.currentHitPoint-damage
    end
    
    

end

function ent.Enemy()

end

function ent:getColors()

    return self.colorR,self.colorG,self.colorB,self.transparency
end

--Draws the box on each frame
function ent:draw()

    
    
    local x,y = self:getPosition()
    local w,h = self:getSize()
    local r,g,b,t=self:getColors()
    love.graphics.setColor(r,g,b,t)
    love.graphics.rectangle( "fill", x, y, w, h )
    self:drawHpBar()
end

function ent.Die()
    


end


function ent:drawHpBar()
    
   
    local initialHp=self.initialHitPoint
    local currentHp=self.currentHitPoint
    local remainingHitPoint=initialHp-currentHp
    
    --green
    love.graphics.setColor(0,220,0,255)
    love.graphics.rectangle( "fill", self.x,self.y-10, initialHp, 5 )
    love.graphics.setColor(220,0,0,255)
    love.graphics.rectangle("fill",self.x+currentHp,self.y-10,initialHp-currentHp,5)
end


function ent:isBoxContainedByRectangle(crosshairX,crosshairY,crosshairW,crosshairH)
      if crosshairX<=self.x and crosshairX+crosshairW>=self.x+self.w then
        if crosshairY<=self.y and crosshairY+crosshairH>=self.y+self.h then
            return true
        end
    end
    return false
end

function ent:isboxcContainedByTriangle()

end

function ent:boxContainedByCircle()


end

return ent;