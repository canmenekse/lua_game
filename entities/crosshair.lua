local ent = ents.Derive("base")

--Loads the crosshair
function ent:load()
	self.x=122
    self.y=125
	self.w = 64
	self.h = 64
    self.BG=false
    self.isActive=false
end

--Set the size of the crosshair
function ent:setSize( w, h )
	self.w = w
	self.h = h
end

--Gets the size of the crosshair
function ent:getSize()
	return self.w, self.h;
end
function ent:setActive()
    self.isActive=true
end


function ent:getStatus()
    return self.isActive
end

function ent:disableCrosshair()
    self.isActive=false
end

--Updates the crosshaires Position
function ent:update(dt)
	if self.isActive then
    self.y = self.y + 32*dt
    end
end

--Draws the crosshair on each frame
function ent:draw()
    --fix that bug
   if self.isActive then 
    self.BG=false
    local w,h = self:getSize()
    love.graphics.setColor(0,0,172,122)
    love.graphics.rectangle( "fill", love.mouse.getX()-crosshairWidth/2, love.mouse.getY()-crosshairHeight/2, crosshairWidth, crosshairHeight )
   end
end

return ent;