local ent = ents.Derive("base")

--Loads the crosshair
function ent:load()
	self.originX=222
    self.originY=222
	self.radius=72
	self.BG=false
    self.isActive=false
end

--Set the size of the crosshair
function ent:setPosition(originX,originY, radius )
	
    self.originX=originX
    self.originY=originY
    self.radius=radius
end




--Gets the size of the crosshair
function ent:getPosition()
	return self.originX,self.originY,self.radius;
end

--Updates the crosshaires Position
function ent:update(dt)
	self.originX=love.mouse.getX()
    self.originY=love.mouse.getY()
    self.radius=crosshairRadius
    --self.y = self.y + 32*dt
    
end

function ent:disableCrosshair()
    self.isActive=false
end

function ent:setActive()
    self.isActive=true
end

--Draws the crosshair on each frame
function ent:draw()
    if self.isActive then
    --fix that bug
       
        self.BG=false
        local originX,originY,radius = self:getPosition()
        love.graphics.setColor(0,0,172,122)
        love.graphics.circle("fill",love.mouse.getX(),love.mouse.getY(),radius)
    end
end

return ent;