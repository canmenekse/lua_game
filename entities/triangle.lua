local ent = ents.Derive("base")

--Loads the box
function ent:load()
	
	self.x1= 64
	self.y1= 64
    self.x2= 74
    self.y2= 128
    self.x3= 256
    self.y3= 256
    self.colorR=1;
    self.colorG=1;
    self.colorB=1;
    self.transparency=1
end

--Set the size of the Triangle
function ent:setPosition(x1,y1,x2,y2,x3,y3)
    self.x1=x1
    self.y1=y1
    self.x2=x2
    self.y2=y2
    self.x3=x3
    self.y3=y3
end


function ent:getColors()

    return self.colorR,self.colorG,self.colorB,self.transparency
end


function ent:getPosition()
    return self.x1,self.y1,self.x2,self.y2,self.x3,self.y3
end

--Updates the Triangle Position
function ent:update(dt)
	self.x1 = self.x1 + 22*dt
    self.x2 = self.x2 + 22*dt
    self.x3 = self.x3 + 22*dt
    
end



function ent:setColor(r,g,b,transparency)
    self.colorR=r
    self.colorG=g
    self.colorB=b
    self.transparency=transparency
end

function ent:spawn(x1,y1,x2,y2,x3,y3,r,g,b,transparency)
    self:setPosition(x1,y1,x2,y2,x3,y3)
    self:setColor(r,g,b,transparency)
    
end

--Draws the box on each frame
function ent:draw()

    
    local vertices={self.x1,self.y1,self.x2,self.y2,self.x3,self.y3}
    
    local r,g,b,t=self:getColors()
    
  
    love.graphics.setColor(r,g,b,t)
    love.graphics.polygon( "fill", vertices )
end

function ent.Die()
    


end


function ent:isTriangleContainedByRectangle(crosshairX,crosshairY,crosshairW,crosshairH)

    local x1,y1,x2,y2,x3,y3=self:getPosition()
    if pointIsInsideBox(crosshairX,crosshairY,crosshairW,crosshairH,x1,y1) and pointIsInsideBox(crosshairX,crosshairY,crosshairW,crosshairH,x2,y2) and pointIsInsideBox(crosshairX,crosshairY,crosshairW,crosshairH,x3,y3) then
        return true
    else
        return false
    end
end

return ent;