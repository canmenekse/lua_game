local ent = ents.Derive("base")

--Loads the box
function ent:load()
	
	self.originX=1
    self.originY=1
    self.initialAndFixedY=self.originY
    self.radius=1
    self.remainingAdd=1
    self. passedCheckpoint=0
    self.creationStamp = love.timer.getTime() + math.random( 0, 128 )
    self.falling=false
    self.initialHitPoint=math.random(1*level/10,250*level/10);
    self.initialHitPointFixed=self.initialHitPoint
    self.circleScore=self.initialHitPoint*120
    
    if math.random(1,72)==72 then
            self.initialHitPoint=math.random(270,1000)
            self.initialHitPointFixed=self.initialHitPoint
            self.remainingAdd=20
        end
    self.currentHitPoint=self.initialHitPoint;
    self.passedCheckpoint=0
    self.ang=0
    self.size=math.random(4,6)
end

--Set the size of the Triangle
function ent:setPosition(originX,originY,radius)
    self.originX=originX
    self.originY=originY
    self.radius=radius
end


function ent:getColors()

    return self.colorR,self.colorG,self.colorB,self.transparency
end


function ent:getPosition()
    return self.originX,self.originY,self.radius
end

--Updates the Triangle Position
function ent:update(dt)
	if self.originX>=widthScreen-2 then
        self.passedCheckpoint=self.passedCheckpoint+1
        self:respawn()
       remainingPlayer=remainingPlayer-1*self.passedCheckpoint
     end
     
     if self.falling==false then
        self.originX=self.originX+(self.size*9)*dt
        self.originY=(self.initialAndFixedY+math.sin(love.timer.getTime()-self.creationStamp)*(self.size*3))
    
    --When the box is falling
    elseif self.falling==true or self.passedCheckPoint>4 then
        self.originX=self.originX+(self.size*9)*dt*12
        self.originY=self.originY+1200*dt
        self.initialAndFixedY=self.initialAndFixedY+32*dt
        self.ang=self.ang+math.pi*0.025*dt
        if self.originY>=300 then
            
            playerScore=playerScore+self.initialHitPointFixed
            enemyCount=enemyCount-1
            remainingPlayer=math.ceil(remainingPlayer+self.remainingAdd/(self.passedCheckpoint+1));
            startBGExplosion(self.originX+512*(self.size/20),self.originY+128*(self.size/20),1)
            self.falling=false
            --print(self.id)
            ents.Destroy(self.id)
        end
    end
    
    --self.originX = self.originX + 22*dt
end



function ent:drawHpBar()
    
   
    local initialHp=self.initialHitPoint
    local currentHp=self.currentHitPoint
    local remainingHitPoint=initialHp-currentHp
    
    --green
    love.graphics.setColor(0,220,0,255)
    love.graphics.rectangle( "fill", self.originX,self.originY-self.radius-10, initialHp, 5 )
    love.graphics.setColor(220,0,0,255)
    love.graphics.rectangle("fill",self.originX+currentHp,self.originY-self.radius-10,initialHp-currentHp,5)
end


function ent:setColor(r,g,b,transparency)
    self.colorR=r
    self.colorG=g
    self.colorB=b
    self.transparency=transparency
end

function ent:spawn(originX,originY,radius,r,g,b,transparency)
    self:setPosition(originX,originY,radius)
    self:setColor(r,g,b,transparency)
    self.initialAndFixedY=self.originY
    
end

--Draws the box on each frame
function ent:draw()

    
    self:drawHpBar()
    local originX,originY,radius=self:getPosition()
    local r,g,b,t=self:getColors()
    
  
    love.graphics.setColor(r,g,b,t)
    love.graphics.circle("fill",originX,originY,radius)
    
end

function ent.Enemy()
    


end


function ent:respawn()

self.originX=math.random(-20,-5)

end


function ent:damage(damage)
    local currentHp=self.currentHitPoint
    if(damage>=currentHp) then
        self.currentHp=0
        self:Fall();
        self.initialHitPoint=0
    
    else
        self.currentHitPoint=self.currentHitPoint-damage
    end

end

function ent:Fall()

    self.falling=true

end


function ent:isCircleContainedByCircle(crosshairOriginX,crosshairOriginY,crosshairRadius)

    local originX,originY,radius=self:getPosition()
    
    local rightXAxisX=originX+radius
    local rightXAxisY=originY
    
    local leftXAxisX=originX-radius
    local leftXAxisY=originY
    
    local topYAxisX=originX
    local topYAxisY=originY+radius
    
    local bottomYAxisX=originX
    local bottomYAxisY=originY-radius
    --print("origin of the crosshair " .. crosshairOriginX.. "," ..crosshairOriginY )
   -- print("radius of the crosshair "..crosshairRadius)
   -- print ( "point 1: ".. rightXAxisX .. ", " ..rightXAxisY)
   -- print ( "point 2: ".. leftXAxisX .. ", " ..leftXAxisY)
   -- print ( "point 3: ".. topYAxisX .. ", " ..topYAxisY)
   -- print ( "point 4: ".. bottomYAxisX .. ", " ..bottomYAxisY)
    
    if  pointIsInsideCircle(rightXAxisX,rightXAxisY,crosshairOriginX,crosshairOriginY,crosshairRadius) and 
        pointIsInsideCircle(leftXAxisX,leftXAxisY,crosshairOriginX,crosshairOriginY,crosshairRadius) and 
        pointIsInsideCircle(topYAxisX,topYAxisY,crosshairOriginX,crosshairOriginY,crosshairRadius) and 
        pointIsInsideCircle(bottomYAxisX,bottomYAxisY,crosshairOriginX,crosshairOriginY,crosshairRadius)
    then
        return true
    else
        return false
    end
end

return ent;