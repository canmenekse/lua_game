ents={}
ents.objects={}
ents.objpath="entities/"
local register= {}
local id=0

--Put the entities to a register
function ents.Startup()
  register["box"]=love.filesystem.load(ents.objpath.."box.lua")
  register["crosshair"]=love.filesystem.load(ents.objpath.."crosshair.lua")
  register["triangle"]=love.filesystem.load(ents.objpath.."triangle.lua")
  
  register["circle"]=love.filesystem.load(ents.objpath.."circle.lua")
  register["circleCrosshair"]=love.filesystem.load(ents.objpath.."circlecrosshair.lua")
end

--Not sure
function ents.Derive(name)
    return love.filesystem.load(ents.objpath..name..".lua")()
end

--Handles the entities creation in the register
function ents.Create(name,BG)
    
    if not BG then
        BG=false
    end
    if register[name] then
        id=id+1;
        local ent=register[name]()
        ent:load()
        ent.id=id;
        ent.type=name;
        ent.BG=BG
        ents.objects[id]=ent;
        --print ("Created Entity");
        return ents.objects[id];
    else
        print("Error: Entity " ..name .. " does not exist!")
        return false
    end
end

--Destroy the Entity with given ID
function ents.Destroy( id )
	if ents.objects[id] then
		if ents.objects[id].Die then
			ents.objects[id]:Die()
		end
		ents.objects[id] = nil
	end
end

--Calls the update of All of the entities
function ents:update(dt)
    for count, ent in pairs(ents.objects) do
        if ent.update then
           ent:update(dt)
        end
        
    end
 end
 
 --Calls the draw of the entities
 function ents:draw(dt)
    for count, ent in pairs(ents.objects) do
      
      if not ent.BG then
        if ent.draw then
           ent:draw(dt)
        end
      end  
    end
 end
 
 
  --Calls the draw of the entities
 function ents:drawBG(dt)
    for count, ent in pairs(ents.objects) do
      
      if ent.BG then
        --print (ent.type)
        if ent.draw then
           ent:draw(dt)
        end
      end  
    end
 end
 
 --To check whether entities are shot and then do something according to that
function ents.shoot(mouseX,mouseY,special)
    
    --Get the currentPosition of the Mouses
    local mousePositionX=mouseX
    local mousePositionY=mouseY
    --Get the coordinates of the crosshair
    
     --When the specialWeapon is used make sure to restart the counter 
     if special==true then
            initialTime=love.timer.getTime()
            specialWeaponAvailable=false
         end
      local weaponSpecificDamage
     --Check all entities
     for i,currentEnt in pairs(ents.objects) do
        
         if currentEnt.Enemy then
            --we assume the entity is not shot yet
            local entityIsHit=false
            if currentEnt.type=="box" and crosshairShape=="rectangle" then
                local crosshairX=mousePositionX-crosshairWidth/2
                local crosshairY=mousePositionY-crosshairHeight/2
               entityIsHit=currentEnt:isBoxContainedByRectangle(crosshairX,crosshairY,crosshairWidth,crosshairHeight)
               
               local crosshairArea=crosshairWidth*crosshairHeight;
                   scaledCrosshairArea=crosshairArea/(1000-level*70)
                   weaponSpecificDamage=50/(scaledCrosshairArea)
            --change these
            elseif currentEnt.type=="box" and crosshairShape=="circle" then
                 entityIsHit=false
            
            elseif currentEnt.type=="circle" and crosshairShape=="circle" then
                local originX,originY,radius=circleCrosshair:getPosition()
                weaponSpecificDamage=1200/(radius*radius)
                
                entityIsHit=currentEnt:isCircleContainedByCircle(originX,originY,radius)
            else
                entityIsHit=false
            end
            
            if entityIsHit then
                local damage;
               
                --When the special weapon is used the damage would be different
                if special==true and ((currentEnt.type=="box" and crosshairShape=="rectangle") or
                    (currentEnt.type=="circle" and crosshairShape=="circle"))
                then
                    damage=120
                    
               elseif  (crosshairShape=="rectangle" and (math.random(1,math.ceil(crosshairWidth*crosshairHeight/2))==22)) or 
                (crosshairShape=="circle" and math.random(1,math.ceil( crosshairRadius* crosshairRadius/2))==22  )
                    then
                    damage=120
                
                elseif special==false then
                
                   damage=weaponSpecificDamage
                 end 
                
                
                --hit the target
                
                currentEnt:damage(damage)
            end
        end
    end
 end
