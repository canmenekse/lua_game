local BGExplosions= {}
--local image=love.graphics.newImage("textures/explosionImage.png")



function startBGExplosion(x,y,magn)
    table.insert(BGExplosions,{x=x,y=y,magn=magn,t=0})
end

function drawBGExplosions()
    for k, ex in pairs(BGExplosions) do
        local sx = (ex.t/(4*ex.magn))
		local sy = (ex.t/(4*ex.magn))
        love.graphics.setColor( 255, 255, 255, 255*(1-(ex.t/(4*ex.magn))) )
        --scale
        local ssx = 0.5 + (sx/2)
		local ssy = sy
		
		--love.graphics.draw( image, ex.x - (256*ssx*0.5), ex.y - (256*ssy), 0, ssx, sst, 0, 0 )
        
        love.graphics.setColor( 255, 255, 255, 180*(1-(ex.t/(4*ex.magn))) )
		love.graphics.circle( "fill", ex.x, ex.y, 2048*(ex.t/(4*ex.magn)), 32)
    end  
end

function updateBGExplosions(dt)
	for k, ex in pairs(BGExplosions) do
		ex.t = ex.t + dt
		if ex.t > 4*ex.magn then
			table.remove(BGExplosions, k)
		end
	end
end