function love.draw()
   --
  
   
    
    --your code    

   
   -- TLfres.transform()
    --draw environment
    drawEnvironment()
    --Get the mouse Coordinates
    printUI()
    drawCrosshair()
    -- TLfres.letterbox(4,3)
    
end


--Update the Game
function love.update(dt)
    TEsound.cleanup()
    announceGameOverWhenNecessary()
    
    if gameState=="Alive" then   
        --Displays the difference between when the initialTime is set(it is first set at startup then it is reset When a specialWeapon is ready)
        local elapsed=love.timer.getTime()-initialTime
        
      --it means such interval elapsed
      if elapsed>specialWeaponWaitDuration then
            --We should check whether the player used any specialWeapon during the countdown
            if usedSpecialWeaponCount~=prevUsedSpecialWeaponCount then
            
            --Put the new values
            specialWeaponWaitDuration=10*usedSpecialWeaponCount
            specialWeaponAvailable=true
            prevUsedSpecialWeaponCount=usedSpecialWeaponCount
            initialTime=love.timer.getTime()
            --print ("Needed specialWeaponWaitDuration is set at 1 ".. specialWeaponWaitDuration  )
            --When elapsed but the user did not use any special Weapon in that duration
            else
            initialTime=love.timer.getTime()
             specialWeaponWaitDuration=10*usedSpecialWeaponCount
            --print ("Needed specialWeaponWaitDuration is set at 2 ".. specialWeaponWaitDuration );
            elapsed=0
            
            end
        
        end
        
        printDelta=specialWeaponWaitDuration-elapsed
   
        --When all enemies are done
        if enemyCount<=0 then
        spawnEnemies()
        end
        --Shoot the boxes
        local x=love.mouse.getX()
        local y=love.mouse.getY()
        ents.shoot( x, y,false )
        --Display explosions
        updateBGExplosions(dt)
        ents:update(dt)
    end
end

--Draws the crosshair
function drawCrosshair()
    local mouseCoordinateX=love.mouse.getX()
    local mouseCoordinateY=love.mouse.getY()
    
    --love.graphics.setColor(12,121,12,255)
    --love.graphics.rectangle("fill",mouseCoordinateX-2,mouseCoordinateY-2,4,4)
end

--Prints whether the expansion control is 'width' or 'height'
function printCrosshairExpansionStatus()
 
    if crosshairShape=="rectangle" then
        love.graphics.print(boxCrosshairState[boxCrosshairStateIndex],200,15)
    else
        love.graphics.print("radius",200,15)
 
    end
end

--Prints whether user can use the special Weapon
function printSpecialWeaponAvailability()
    if specialWeaponAvailable then
        
         love.graphics.print("SpecialWeapon : Yes" ,1000,15)
    
    else
    
        love.graphics.print("SpecialWeapon : N/A" ,1000,15)
    end
end

--Prints the remaining duration until Super Weapon
function printUntilSuperWeapon()
    
    love.graphics.print("Available In : "..printDelta ,1200,15)
end

--Prints the UI
function printUI()
    love.graphics.setColor(50,120,255)
    printCrosshairExpansionStatus()
    love.graphics.print("Remaining : "..remainingPlayer,300,15)
    love.graphics.print("Score : "..playerScore,500,15)
    love.graphics.print("Enemies remaining: "..enemyCount,700,15)
    love.graphics.print("Level : " ..level ,900,15)
    printSpecialWeaponAvailability()
    printUntilSuperWeapon()

end

--Draw Sky
function drawSky()
    love.graphics.setColor( skyR, skyG, skyB, skyT )
	love.graphics.rectangle( "fill", 0, 0, widthScreen, heightScreen/2 )
end

--Draw Land
function drawLand()
    love.graphics.setColor(landR,landG,landB,landT)
    love.graphics.rectangle("fill",0,heightScreen/2+1,widthScreen,heightScreen-(heightScreen/2+1))
end

--Draws the grass like ground in the ground
function drawGround()
    love.graphics.draw(imageGround,(800-1024)/2,300-64,0,1,1,0,0)
    
end

--Draw Environment
function drawEnvironment()
    if level6 then
    drawDarkSky()
    
    
    else
    drawSky()
    end
    
    drawBGExplosions()
    ents:drawBG()
    drawLand()
    --drawGround()
    if gameState=="Alive" then   
    ents:draw()
    end
end   

function drawDarkSky()
     love.graphics.setColor( 0, 0, 0, 255 )
	love.graphics.rectangle( "fill", 0, 0, 1360, 300 )
end
    
--When the game launches it loads the necessary items
function love.load()
    require("entities")
    require("gameExplosionEffects")
    require("TEsound")
    --require("TLfres")
    volume=1
    loadBackgroundTextures()
    TEsound.playLooping("audio/music.ogg","bm",nil,volume)
   --TLfres.setScreen({w=1280, h=1024, full=false, vsync=false, aa=0},1366,false,true) 
    ents.Startup();
    --Globals
        initialTime=love.timer.getTime()
        --crosshair 
            modes = love.graphics.getModes()
            table.sort(modes, function(a, b) return a.width*a.height < b.width*b.height end)   -- sort from smallest to largest
            --print (modes[#modes].width ..",".. modes[#modes].height)
            
           idealWidth=modes[#modes].width
           idealHeight=modes[#modes].height
           love.graphics.setMode( idealWidth,idealHeight,true)
           widthScreen=love.graphics.getWidth()
            heightScreen=love.graphics.getHeight()
           boxCrosshairState={}
           boxCrosshairState[1]="diagonal"
           boxCrosshairState[2]="width"
           boxCrosshairState[3]="height"
           boxCrosshairStateIndex=1
            skyR=198
            skyG=241
            skyB=255
            skyT=255
            landR=103
            landG=164
            landB=22
            landT=255
            
            crosshairWidth=64
            crosshairHeight=64
           
        --mouseControl
            modifyWidth=true
        --Score Related
            playerScore=0
            remainingPlayer=1
            level=1
            enemyCount=22
            spawnedEnemyCount=enemyCount
        --Mechanic Related
            specialWeaponAvailable=true
            usedSpecialWeaponCount=1
        --convenience
            printDelta=10
            specialWeaponWaitDuration=10
            prevUsedSpecialWeaponCount=1
        --gameState
            gameState="Alive"
     --------------------      
           
    spawn()
    rectangleCrosshair=ents.Create("crosshair",128,272,false)
    rectangleCrosshair:setActive()
    circleCrosshair=ents.Create("circleCrosshair",128,272,false)
    circleCrosshair:disableCrosshair()
    crosshairShape="rectangle"
    crosshairRadius=72
    
 end

 --Initially Spawn enemies
function spawn()
     for i=1,enemyCount do
        if i%2==0 then
        
        local box=ents.Create("box",false)
        box:spawn(math.random(0,200),math.random(1,720),math.random(1,200),math.random(1,250),math.random(1,255),math.random(1,255),math.random(1,255),math.random(120,255))
        
        else
        local circle=ents.Create("circle",false)
        circle:spawn(math.random(0,200),math.random(1,heightScreen-72),math.random(22,76),math.random(1,250),math.random(1,250),math.random(1,250),math.random(120,255))
        end
      
      end
end

--Spawn enemies when the level is completed
function spawnEnemies()
     enemyCount=spawnedEnemyCount+level*2
     level=level+1
      skyR=math.random(1,255)
      skyG=math.random(1,255)
      skyB=math.random(1,255)
      skyT=math.random(1,255)
        landR=math.random(1,255)
      landG=math.random(1,255)
      landB=math.random(1,255)
      landT=math.random(220,255)
     spawnedEnemyCount=enemyCount
     --Spawn the boxes
     spawn();
end

--Loads the background textures
function loadBackgroundTextures()
    --imageCloud=love.graphics.newImage("textures/cloud.png")
    --imageGround=love.graphics.newImage("textures/ground.png")
end
--When the remaining Player is depleted game is finished
function announceGameOverWhenNecessary()
    if remainingPlayer<=0 then
        gameState="Over"
     end
end

function changeCrosshair()
    if crosshairShape=="rectangle" then
    
        rectangleCrosshair:disableCrosshair()
        circleCrosshair:setActive()
        crosshairShape="circle"
    
    
    
    else
        rectangleCrosshair:setActive()
        circleCrosshair:disableCrosshair()
        crosshairShape="rectangle"
    end
end



--Check whether a single coordinate is between the start and end point in an axis in the graphic
function pointBetweenTheAxes(point,axisComparePoint,axisMagnitude)
    if point >axisComparePoint and point<axisComparePoint+axisMagnitude then
        return true
    else
        return false
    end
end

--When mouse is pressed
function love.mousepressed( x, y, button )
	
    --Mouse wheel Up
    if button=="wu" then
      if crosshairShape=="rectangle" then
           if boxCrosshairState[boxCrosshairStateIndex]=="width"  then
               crosshairWidth=crosshairWidth+15
           elseif boxCrosshairState[boxCrosshairStateIndex]=="height" then
                crosshairHeight=crosshairHeight+15
           
           
           else
            crosshairWidth=crosshairWidth+15
            crosshairHeight=crosshairHeight+15
           end 
      
      
      else
            crosshairRadius=crosshairRadius+7
      end
    --Mouse wheel down
    elseif button=="wd" then
        if crosshairShape=="rectangle" then    
            if  boxCrosshairState[boxCrosshairStateIndex]=="width" and crosshairWidth-15>=0 then
                crosshairWidth=crosshairWidth-15
           elseif crosshairHeight-15>=0 and boxCrosshairState[boxCrosshairStateIndex]=="height" then 
                crosshairHeight=crosshairHeight-15
           
           elseif crosshairHeight-15>=0 and crosshairWidth-15>=0 then
                crosshairWidth=crosshairWidth-15
                crosshairHeight=crosshairHeight-15
           end
        elseif crosshairRadius-7>=0 then
        crosshairRadius=crosshairRadius-7

        end
     --R button of the mouse  
    elseif button=="r" then
            if modifyWidth then
                modifyWidth=false
            else
                modifyWidth=true
            end
    
    elseif button=="l" then
    changeCrosshair()
    end
end

--Keyboard Press events
function love.keypressed(key)
  if key=="u" and (volume+0.1)<=1 then
    volume=volume+0.1
    TEsound.volume("bm", volume)
    
    
  elseif key=="d" and (volume-0.1)>=0 then
    volume=volume-0.1
    TEsound.volume("bm", volume)
  end
  
  
  if crosshairShape=="rectangle" then 
   if key == "1" then
      boxCrosshairStateIndex=1
      
   
   elseif key == "2" then
      boxCrosshairStateIndex=2
   
   elseif key == "3" then
      boxCrosshairStateIndex=3
   
   elseif key == "4" then
      print("Size of the Crosshair is changed")
      crosshairWidth=64
      crosshairHeight=64
   
   elseif key == "5" then
      print("Size of the Crosshair is changed")
      crosshairWidth=256
      crosshairHeight=256
   
   elseif key == "6" then
      print("Size of the Crosshair is changed")
      crosshairWidth=512
      crosshairHeight=512
   
     
   elseif key == "7" then
      print("Size of the Crosshair is changed")
      crosshairWidth=1024
      crosshairHeight=1024
   
   elseif key == "8" then
      print("Size of the Crosshair is changed")
      crosshairWidth=2048
      crosshairHeight=2058
    end
  end  
    
   if key=="q" then
         local x=love.mouse.getX()
        local y=love.mouse.getY()
        ents.shoot(x,y, true)
   elseif key == "escape" then
    love.event.quit()
   
    
    
    
    elseif key=="r" then
        resetGame()
   --When the special Weapon Key is used
   elseif key=="s" and specialWeaponAvailable then
     local x=love.mouse.getX()
     local y=love.mouse.getY()
   --No matter the aim of the specialWeapon is correct or not player uses the specialWeapon
    usedSpecialWeaponCount=usedSpecialWeaponCount+1;
    ents.shoot(x,y, specialWeaponAvailable)
    end
    
  
    

   
    
end

function destroyAllEnemies()
  for i, ent in pairs(ents.objects) do
    ents.Destroy(ent.id)
   end
end


function resetGame()
        gameState="Over"
        destroyAllEnemies()
        --Globals
        initialTime=love.timer.getTime()
        --crosshair 
        
              rectangleCrosshair=ents.Create("crosshair",128,272,false)
    rectangleCrosshair:setActive()
    circleCrosshair=ents.Create("circleCrosshair",128,272,false)
    circleCrosshair:disableCrosshair()
    crosshairShape="rectangle"
    crosshairRadius=72
             skyR=198
            skyG=241
            skyB=255
            skyT=255
              landR=103
            landG=164
            landB=22
            landT=255
            crosshairShape="rectangle"
            crosshairWidth=64
            crosshairHeight=64
        --mouseControl
            modifyWidth=true
        --Score Related
            playerScore=0
            remainingPlayer=1
            level=1
            enemyCount=22
            spawnedEnemyCount=enemyCount
        --Mechanic Related
            specialWeaponAvailable=true
            usedSpecialWeaponCount=1
        --convenience
            printDelta=10
            specialWeaponWaitDuration=10
            prevUsedSpecialWeaponCount=1
        --gameState
            gameState="Alive"
     --------------------      
           
    spawn()
   
end

--Check whether a single Point is in a box
function pointIsInsideBox(crosshairX,crosshairY,crosshairW,crosshairH,pointX,pointY)
    if pointX>=crosshairX and pointX<=crosshairX+crosshairW then
        if pointY>crosshairY and pointY<crosshairY+crosshairH then
            return true
        end
    else
        return false
    end
    
end

function pointIsInsideCircle(pointX,pointY,originX,originY,radius)

    --print ("pointX-originX : "..pointX -originX .. "when it is squared " .. (pointX-originX)*(pointX-originX))
    --print ("pointY-originY : "..pointY -originY .. "when it is squared " .. (pointY-originY)*(pointY-originY))
    local distanceSquared=( (pointX-originX)*(pointX-originX) + (pointY-originY)* (pointY-originY) )
    
   -- print (distanceSquared.. "<=" .. radius*radius)
    
    if(distanceSquared<=(radius*radius)) then
        return true
    else
        return false
    end

end

